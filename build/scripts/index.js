(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["index"],{

/***/ "./scripts/index.js":
/*!**************************!*\
  !*** ./scripts/index.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);

jquery__WEBPACK_IMPORTED_MODULE_0___default()(function () {
  console.log('Exercise initialized, good luck ;) ');
}); // Fonction focus du premier element du formulaire lorsqu'il est ouvert

jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).ready(function () {
  jquery__WEBPACK_IMPORTED_MODULE_0___default()('input:visible:enabled:first').focus();
}); // Presser ESCAPE pour fermer le formulaire

jquery__WEBPACK_IMPORTED_MODULE_0___default()(document).keydown(function (event) {
  if (event.keyCode == 27) {
    jquery__WEBPACK_IMPORTED_MODULE_0___default()('#address-modal').hide();
  }
}); // Fonction qui fait que le focus n'aille pas sur la barre de navigation

jquery__WEBPACK_IMPORTED_MODULE_0___default()('body').on('keydown', function (e) {
  var jqTarget = jquery__WEBPACK_IMPORTED_MODULE_0___default()(e.target);

  if (e.keyCode == 9) {
    var jqVisibleInputs = jquery__WEBPACK_IMPORTED_MODULE_0___default()(':input:visible');
    var jqFirst = jqVisibleInputs.first();
    var jqLast = jqVisibleInputs.last();

    if (!e.shiftKey && jqTarget.is(jqLast)) {
      e.preventDefault();
      jqFirst.focus();
    } else if (e.shiftKey && jqTarget.is(jqFirst)) {
      e.preventDefault();
      jqLast.focus();
    }
  }
});

/***/ })

},[["./scripts/index.js","runtime","vendors~index"]]]);
//# sourceMappingURL=index.js.map