import $ from 'jquery';

$(function() {
  console.log('Exercise initialized, good luck ;) ');
});


// Fonction focus du premier element du formulaire lorsqu'il est ouvert
$(document).ready(function() {
  $('input:visible:enabled:first').focus();
});

// Presser ESCAPE pour fermer le formulaire
$(document).keydown(function(event) { 
  if (event.keyCode == 27) { 
    $('#address-modal').hide();
  }
});

// Fonction qui fait que le focus n'aille pas sur la barre de navigation
$('body').on('keydown', function (e) {
  var jqTarget = $(e.target);
  if (e.keyCode == 9) {

      var jqVisibleInputs = $(':input:visible');
      var jqFirst = jqVisibleInputs.first();
      var jqLast = jqVisibleInputs.last();

      if (!e.shiftKey && jqTarget.is(jqLast)) {
          e.preventDefault();
          jqFirst.focus();
      } else if (e.shiftKey && jqTarget.is(jqFirst)) {
          e.preventDefault();
          jqLast.focus();
      }
  }
});